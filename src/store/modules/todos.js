// import axios from 'axios';

const state = {
    todos: [
        {
            id: 1,
            title: 'Todo one'
        },
        {
            id: 2,
            title: 'Todo two'
        },
        {
            id: 3,
            title: 'Todo three'
        }
    ]
};
const getters = {
    allTodos: (state) => state.todos
};
const actions = {
//     async fetchTodos({commit}) { 
        
//     const response = await axios.get(
//         'https://jsonplaceholder.typicode.com/todos'
//         );
//         commit('setTodos', response.data);   
//     },
//     async addTodo({ commit }, titleL) {
//         const response = await axios.post('https://jsonplaceholder.typicode.com/todos', {
//             completed: false
//         });
//     }

};
const mutations = {};
export default {
    state,
    getters,
    actions,
    mutations
}